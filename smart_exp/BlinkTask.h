#ifndef __BLINKTASK__
#define __BLINKTASK__

#include "LimitedTask.h"
#include "Led.h"

#define ERROR_TIME 2000

class BlinkTask: public LimitedTask {

  Light* led;
  enum { ON, OFF} state;
  bool limited;

public:
  void init(int period, Light* l);  
  void tick();
  
  void setLimited(bool value) {
    limited = value;
  }
  
  bool isLimited() {
    return limited;
  }
};

#endif
