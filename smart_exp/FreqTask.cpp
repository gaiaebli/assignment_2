#include "FreqTask.h"
#include "MsgService.h"
  
void FreqTask::init(int period, ServoMotor* motor){
  Task::init(period);
  this->motor = motor;
  motor->on();
}
  
void FreqTask::tick(){
  float distance = this->getDistance(); //calcolo la distanza
  MsgService.sendMsg(String(distance)); //mando la distanza al Viewer
  int vel = distance/(getRunningTime()/1000); //calcolo la velocità
  motor->setPosition(180*vel/MAX_VEL); // setto il servo
  if (this->getRunningTime() >= MAX_TIME) {
    this->stopRunning();
  }
}

float FreqTask::computeSoundVel() {
  return 331.45 + 0.62 * analogRead(TEMP);
}

float FreqTask::getDistance() {
  /* invio impulso */
  digitalWrite(SONAR_TRIG, LOW);
  delayMicroseconds(3);
  digitalWrite(SONAR_TRIG, HIGH);
  delayMicroseconds(5);
  digitalWrite(SONAR_TRIG, LOW);

  /* ricevi l’eco */
  float tUS = pulseIn(SONAR_ECHO, HIGH);
  float t = tUS / 1000.0 / 1000.0 / 2;
  float d = t * this->computeSoundVel();
  return d;
}
