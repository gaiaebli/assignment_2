#include "BlinkTask.h"
  
void BlinkTask::init(int period, Light* l){
  Task::init(period);
  led = l; 
  state = OFF;
}
  
void BlinkTask::tick(){
  switch (state){
    case OFF:
      led->switchOn();
      state = ON; 
      break;
    case ON:
      led->switchOff();
      state = OFF;
      break;
  }
  if (this->limited && this->getRunningTime() >= ERROR_TIME) {
    this->stopRunning();
  }
}
