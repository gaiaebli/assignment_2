#include "ExperimentTask.h"
  
void ExperimentTask::init(int period, Light* greenLed, Light* redLed, BlinkTask* redBlinkTask, ActiveTask* activeTask, FreqTask* freqTask, IdleModeTask* idleModeTask){
  Task::init(period);
  this->greenLed = greenLed;
  this->redLed = redLed;
  this->redBlinkTask = redBlinkTask;
  this->activeTask = activeTask;
  this->freqTask = freqTask;
  this->idleModeTask= idleModeTask;
}
  
void ExperimentTask::tick(){
  switch (expStatus){
    case ACTIVE: {
        if(!this->activeTask->isRunning()) { //vado in modalità risparmio energetico
          idleModeTask->run();
          this->greenLed->switchOff();
          MsgService.sendMsg("SUSPENDED");
          expStatus = SUSPENDED;
        }
      break;
    }
    case EXECUTING: {
      if(!this->freqTask->isRunning()) { //l'esperimento è finito
        this->redBlinkTask->setLimited(false);
        this->redBlinkTask->run();
        MsgService.sendMsg("ENDED");
        expStatus = ENDED;
      }
      break;
    }
    case SUSPENDED: {
      if (!idleModeTask->isRunning()) { //torno nello stato active
        this->greenLed->switchOn();
        MsgService.sendMsg("ACTIVE");
        this->activeTask->run();
        expStatus = ACTIVE;
      }
      break;
    }
    case ENDED: {
      Msg* msg = MsgService.receiveMsg();
      if (msg->getContent() == "OK") { //solo se il messaggio dal Viewer è OK posso tornare nello stato active
        delete msg; //cancello il messaggio
        this->greenLed->switchOn();
        this->redLed->switchOff();
        this->redBlinkTask->stopRunning();
        MsgService.sendMsg("ACTIVE");
        this->activeTask->run();
        expStatus = ACTIVE;
      }
      break;
    }
    case FAILED: {
      if (!this->redBlinkTask->isRunning()) { //torno in stato active quando il blinking del led rosso è finito
        this->redLed->switchOff();
        MsgService.sendMsg("ACTIVE");
        this->activeTask->run();
        expStatus = ACTIVE;
      }
      break;
    }
  }
}
