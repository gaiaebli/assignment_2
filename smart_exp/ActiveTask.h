#ifndef __ACTIVETASK__
#define __ACTIVETASK__

#include "Arduino.h"
#include "LimitedTask.h"
#include "Led.h"

#define SLEEP_TIME 5000
#define POT A0

class ActiveTask: public LimitedTask {

  short freq;

  public:
    void init(int period);
    void tick();
    short getFrequency() {
      return freq;
    }
};

#endif
