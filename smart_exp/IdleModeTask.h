#ifndef __IDLEMODETASK__
#define __IDLEMODETASK__

#include <avr/sleep.h>
#include <avr/power.h>
#include "LimitedTask.h"
#include "Arduino.h"

#define PIR 4

class IdleModeTask: public LimitedTask {

public: 
  void init(int period);  
  void tick();

  void run() {
    Task::run();
    sleep();
  }
  
  void stopRunning() {
    LimitedTask::stopRunning();
    wakeup();
  }

  void sleep() {
    set_sleep_mode(SLEEP_MODE_IDLE);
    sleep_enable();
    power_adc_disable();
    power_spi_disable();
    power_timer0_disable();
    power_timer2_disable();
    power_twi_disable();
    sleep_mode();
    sleep_disable();
    power_all_enable();
  }

  void wakeup() {
    sleep_disable();
    power_all_enable();
  }
};

#endif
