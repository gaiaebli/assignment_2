/*******************************
* EBLI GAIA - matr. 0000882742 *
********************************/

#include "Scheduler.h"
#include "BlinkTask.h"
#include "MsgService.h"
#include "FreqTask.h"
#include "ActiveTask.h"
#include "IdleModeTask.h"
#include "ExperimentTask.h"
#include "Led.h"
#include "servo_motor_impl.h"

#define GREEN_LED 11
#define RED_LED 12
#define BSTART 2
#define BSTOP 3
#define SERVO 9
#define CALIBRATION_TIME_SEC 10
#define SCHED_PERIOD 20
#define MIN_PERIOD 40 // 1000 ms / 25 Hz = 40 ms

Light* greenLed;
Light* redLed;
ServoMotor* motor;
ActiveTask* activeTask;
BlinkTask* redBlinkTask;
ExperimentTask* experimentTask;
FreqTask* freqTask;
IdleModeTask* idleModeTask;
Scheduler scheduler;
status expStatus;

void setup() {
  pinMode(GREEN_LED, OUTPUT);
  pinMode(RED_LED, OUTPUT);
  pinMode(BSTART, INPUT);
  pinMode(BSTOP, INPUT);
  pinMode(PIR, INPUT);
  pinMode(SONAR_TRIG, OUTPUT);
  pinMode(SONAR_ECHO, INPUT);

  attachInterrupt(digitalPinToInterrupt(BSTART), starting, HIGH);
  attachInterrupt(digitalPinToInterrupt(BSTOP), stopping, HIGH);

  scheduler.init(SCHED_PERIOD);
  MsgService.init();
  
  greenLed = new Led(GREEN_LED);
  redLed = new Led(RED_LED);
  motor = new ServoMotorImpl(SERVO);

  freqTask = new FreqTask();

  activeTask = new ActiveTask();
  activeTask->init(SCHED_PERIOD);

  idleModeTask = new IdleModeTask();
  idleModeTask->init(SCHED_PERIOD);
  
  redBlinkTask = new BlinkTask();
  redBlinkTask->init(500, redLed);

  experimentTask = new ExperimentTask();
  experimentTask->init(SCHED_PERIOD, greenLed, redLed, redBlinkTask, activeTask, freqTask, idleModeTask);

  scheduler.addTask(experimentTask);
  scheduler.addTask(activeTask);
  scheduler.addTask(freqTask);
  scheduler.addTask(redBlinkTask);
  scheduler.addTask(idleModeTask);

  MsgService.sendMsg("Calibrating sensor");
  for (int i = 0; i < CALIBRATION_TIME_SEC; i++) {
    delay(1000);
  }
  MsgService.sendMsg("PIR SENSOR READY");
  MsgService.sendMsg("ACTIVE");

  greenLed->switchOn();
  redLed->switchOff();

  experimentTask->run();
  activeTask->run();
}

void loop() {
  scheduler.schedule();
}

//funzione associata al pulsante BSTART
void starting() {
  delayMicroseconds(30000); //previene il bouncing del pulsante
  if (experimentTask->getStatus() == ACTIVE && digitalRead(PIR) == HIGH && experimentTask->isRunning()) { // se posso far partire l'esperimento
    activeTask->stopRunning();
    redLed->switchOn();
    int period = MIN_PERIOD + (SCHED_PERIOD * activeTask->getFrequency()); //calcolo il periodo sulla base della frequenza
    MsgService.sendMsg(String(period)); //mando il periodo al Viewer
    freqTask->init(period, motor); //inizializzo il task che fa le rilevazioni
    MsgService.sendMsg("EXECUTING");
    freqTask->run();
    experimentTask->setStatus(EXECUTING);
  } else if (experimentTask->getStatus() == ACTIVE && digitalRead(PIR) == LOW) { //se non posso far partire l'esperimento
    activeTask->stopRunning();
    redBlinkTask->setLimited(true); //blinking che ha un tempo massimo
    redBlinkTask->run();
    MsgService.sendMsg("ERROR");
    experimentTask->setStatus(FAILED);
  }
}

//funzione associata al pulsante BSTOP
void stopping() {
  delayMicroseconds(30000); //previene il bouncing del pulsante
  if (experimentTask->getStatus() == EXECUTING) {
    freqTask->stopRunning();
    redBlinkTask->setLimited(false); //blinking che non ha limiti di tempo
    redBlinkTask->run();
    MsgService.sendMsg("ENDED");
    experimentTask->setStatus(ENDED);
  }
}
