#ifndef __TASK__
#define __TASK__

class Task {

    int myPeriod;
    int timeElapsed;
    bool running;

  public:
    virtual void init(int period) {
      myPeriod = period;
      timeElapsed = 0;
      running = false;
    }

    virtual void tick() = 0;

    virtual bool updateAndCheckTime(int basePeriod) {
      timeElapsed += basePeriod;
      if (timeElapsed >= myPeriod) {
        timeElapsed = 0;
        return true;
      } else {
        return false;
      }
    }

    virtual void run() {
      running = true;
    }

    virtual void stopRunning() {
      running = false;
    }

    bool isRunning() {
      return running;
    }
};

#endif
