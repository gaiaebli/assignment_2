#ifndef __LIMITEDTASK__
#define __LIMITEDTASK__

#include "Task.h"
#include "MsgService.h"

//classe per i task che si concludono dopo un certo periodo di tempo
class LimitedTask: public Task {
  
  unsigned int runningTime;

  public:

    virtual bool updateAndCheckTime(int basePeriod){
      if (isRunning()) {
        runningTime += basePeriod;
      }
      Task::updateAndCheckTime(basePeriod);
    }
  
    virtual void stopRunning(){
      Task::stopRunning();
      runningTime = 0;
    }

    unsigned long getRunningTime() {
      return runningTime;
    }

};

#endif
