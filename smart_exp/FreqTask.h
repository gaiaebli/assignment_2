#ifndef __FREQTASK__
#define __FREQTASK__

#include "LimitedTask.h"
#include "servo_motor_impl.h"

#define MAX_TIME 20000
#define MAX_VEL 10 // cm/sec

#define TEMP A3
#define SONAR_TRIG 8
#define SONAR_ECHO 7

class FreqTask: public LimitedTask {

  ServoMotor* motor;

public: 
  void init(int period, ServoMotor* motor);  
  void tick();
  void stopRunning() {
    LimitedTask::stopRunning();
    motor->off();
  }

private:
  float computeSoundVel();
  float getDistance();
};

#endif
