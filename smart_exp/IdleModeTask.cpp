#include "IdleModeTask.h"
  
void IdleModeTask::init(int period){
  Task::init(period);
}
  
void IdleModeTask::tick(){
  if (digitalRead(PIR) == HIGH) { //se il pir rileva qualcosa esco dal risparmio energetico
    this->stopRunning();
  }
}
