#ifndef __EXPERIMENTTASK__
#define __EXPERIMENTTASK__

#include "Task.h"
#include "MsgService.h"
#include "Led.h"
#include "BlinkTask.h"
#include "ActiveTask.h"
#include "FreqTask.h"
#include "IdleModeTask.h"

extern enum status {ACTIVE, EXECUTING, ENDED, FAILED, SUSPENDED} expStatus;

class ExperimentTask: public Task {

  Light* greenLed;
  Light* redLed;
  BlinkTask* redBlinkTask;
  ActiveTask* activeTask;
  FreqTask* freqTask;
  IdleModeTask* idleModeTask;

public:
  void init(int period, Light* greenLed, Light* redLed, BlinkTask* redBlinkTask, ActiveTask* activeTask, FreqTask* freqTask, IdleModeTask* idleModeTask);  
  void tick();
  
  void setStatus(status value) {
    expStatus = value;
  }
  
  status getStatus() {
    return expStatus;
  }
};

#endif
