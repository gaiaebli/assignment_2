#include "ActiveTask.h"
  
void ActiveTask::init(int period){
  Task::init(period);
}

//FINIRE
void ActiveTask::tick(){
  /*in questo modo so quale dei 48 intervalli individuati dai multipli di 20 (il periodo dello scheduler) tra 40 e 1000 ms cerco,
    poi siccome il periodo è inversamente proporzionale alla frequenza devo prendere il corrispondente tramite la sottrazione.
    ESEMPIO: se il potenziometro restituisce 0 significa che voglio una frequenza davvero bassa (1 Hz) e perciò un periodo molto lungo (1000 ms), quindi
    siccome il calcolo del periodo viene fatto come 1000 ms / 25 Hz + 20 ms * freq, il risultato sarà 40 ms + 20 ms * 48 = 1000 ms = 1 sec
  */
  freq = 48 - (analogRead(POT) / 21); //21 è circa il numero di valori compresi in ciascuno dei 48 intervalli del potenziometro
  if (this->getRunningTime() >= SLEEP_TIME) {
    this->stopRunning();
  }
}
