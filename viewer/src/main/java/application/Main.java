package application;

import controllers.ControllerImpl;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * This class represent the Main class of the JavaFX-based application.
 */
public final class Main extends Application {
    
    private ControllerImpl controller;

    @Override
    public void start(final Stage stage) throws Exception {
        FXMLLoader loader = new FXMLLoader(ClassLoader.getSystemResource("layouts/view.fxml"));
        final Parent root = loader.load();
        final Scene scene = new Scene(root, 1000,800);
        this.controller = new ControllerImpl(loader.getController());
        // Stage configuration
        stage.setTitle("Smart Experiment");
        stage.setScene(scene);
        stage.centerOnScreen();
        stage.setOnCloseRequest(t -> {
            Platform.exit();
            System.exit(0);
        });
        stage.show();
        this.controller.start();
        
    }

    /**
     * 
     * @param args unused
     * @throws Exception 
     */
    public static void main(final String[] args) throws Exception {
        launch();
    }

}
