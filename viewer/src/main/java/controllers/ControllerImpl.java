package controllers;

import java.io.IOException;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import model.Model;
import model.ModelImpl;

public class ControllerImpl implements Controller {
    
    private final Model model;
    private final ViewController viewController;
    private Stage popup;
    private boolean flagFreq;

    public ControllerImpl(final ViewController view) throws Exception {
        this.model = new ModelImpl();
        this.viewController = view;
        this.flagFreq = false;
        this.initializePopup();
    }

    /**
     * {@inheritDoc}
     */
    public void start() {
        new Thread (() -> {
            /* attesa necessaria per fare in modo che Arduino completi il reboot */
            Platform.runLater(() -> {
                this.updateViewStatus("Waiting Arduino for rebooting...");
            });
            this.threadSleep(4000);
            while (true) {
                final String msg;
                try {
                    msg = this.model.getMessage();
                    if (this.isNotANum(msg)) { //se non è un numero
                        this.stringMsg(msg);                        
                    } else if (this.isIntegerNum(msg)) { //se il numero è intero
                        this.integerMsg(msg);
                    } else if (this.isDecimalNum(msg)){ //se il numero è decimale
                        this.decimalMsg(msg);
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                this.threadSleep(20);
            }
        }).start();
    }
    
    /**
     * {@inheritDoc}
     */
    public void endExperiment() {
        this.flagFreq = false;
        this.model.sendMessage("OK");
        this.model.reset();
        this.viewController.reset();
    }
    
    /**
     * Updates the view's charts.
     */
    private void updateViewChart() {
        final double currentTime = this.model.getDetManager().getTimeInSeconds();
        final double position = this.model.getDetManager().getPosition();
        final double speed = this.model.getDetManager().getSpeed();
        final double acceleration = this.model.getDetManager().getAcceleration();
        Platform.runLater(() -> {
            this.viewController.updateCharts(currentTime, position, speed, acceleration);
        });
    }
    
    /**
     * Updates the view's status label.
     * @param status
     *      the label's status
     */
    private void updateViewStatus(final String status) {
        Platform.runLater(() -> {
            this.viewController.updateStatus(status);
        });
    }
    
    /**
     * Creates the popup that appears when the experiment ends.
     * @throws IOException
     *      if the fxml file is mispelled or doesn't exist
     */
    private void initializePopup() throws IOException {
        this.popup = new Stage();
        final FXMLLoader loader = new FXMLLoader(ClassLoader.getSystemResource("layouts/popup.fxml"));
        loader.setController(new PopupControllerImpl(this));
        final Parent root = loader.load();
        final Scene scene = new Scene(root, 300, 200);
        this.popup.setTitle("Confirm");
        this.popup.setScene(scene);
        this.popup.setAlwaysOnTop(true);
        this.popup.close();
    }
    
    /**
     * Causes the currently executing thread to sleep for the specified number of milliseconds.
     * @param millis
     *      time in milliseconds
     */
    private void threadSleep(final long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Checks if a string is an integer.
     * @param string
     *      string to check
     * @return true if the string is an integer
     */
    private boolean isIntegerNum(final String string) {
        return string.matches("-?\\d+");
    }
    
    /**
     * Checks if a string is an decimal number.
     * @param string
     *      string to check
     * @return true if the string is an decimal number
     */
    private boolean isDecimalNum(final String string) {
        return string.matches("[-+]?[0-9]*\\.?[0-9]+");
    }
    
    /**
     * Checks if a string is not an number.
     * @param string
     *      string to check
     * @return true if the string is not a number
     */
    private boolean isNotANum(final String string) {
        return string.matches("[a-zA-Z]+");
    }
    
    /**
     * What happens if a string is just a string.
     * @param msg
     */
    private void stringMsg(final String msg) {
        if(msg.equals("ENDED")) {
            Platform.runLater(() -> {
                this.popup.show();
            });
        } else {
            this.updateViewStatus(msg);
        }
    }
    
    /**
     * What happens if a string is an integer.
     * @param msg
     */
    private void integerMsg(final String msg) {
        if (!this.flagFreq) { //solo la frequenza arriva come intero, ma uso un falg per essere sicura che non venga sovrascritta
            this.model.getDetManager().setPeriod(Integer.parseInt(msg));
            this.flagFreq = true;
        }
    }
    
    /**
     * What happens if a string is a decimal number.
     * @param msg
     */
    private void decimalMsg(final String msg) {
        this.model.getDetManager().addData(Double.parseDouble(msg)); //aggiorno il model
        this.updateViewChart(); //aggiorno la view
    }
}
