package controllers;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.Label;

/**
 * The Controller related to the main.fxml GUI.
 *
 */
public final class ViewControllerImpl implements Initializable, ViewController{
    
    private Series<Double,Double> positions;
    private Series<Double,Double> speed;
    private Series<Double,Double> acceleration;
    
    @FXML
    private LineChart<Double,Double> positionChart;
    
    @FXML
    private LineChart<Double,Double> speedChart;
    
    @FXML
    private LineChart<Double,Double> accelerationChart;
    
    @FXML
    private Label label;
    
    /*
     * Updates the positions' chart with the current value.
     */
    private void updatePositionChart(final double time, final double position) {
        this.positions.getData().add(new Data<>(time, position));
    }
    
    /*
     * Updates the speed's chart with the current value.
     */
    private void updateSpeedChart(final double time, final double speed) {
        this.speed.getData().add(new Data<>(time, speed));
    }
    
    /*
     * Updates the acceleration's chart with the current value.
     */
    private void updateAccelerationChart(final double time, final double acceleration) {
        this.acceleration.getData().add(new Data<>(time, acceleration));
    }
    
    /*
     * Clears the three series.
     */
    private void clearSeries() {
        this.positions.getData().clear();
        this.speed.getData().clear();
        this.acceleration.getData().clear();
    }
    
    /*
     * Clears the three charts.
     */
    private void clearCharts() {
        this.positionChart.getData().clear(); //tolgo la lista dal grafico e la riaggiungo svuotata
        this.positionChart.getData().add(this.positions);
        this.speedChart.getData().clear();
        this.speedChart.getData().add(this.speed);
        this.accelerationChart.getData().clear();
        this.accelerationChart.getData().add(this.acceleration);
    }
    
    /**
     * Sets the three charts' animated value.
     * @param value
     */
    private void setAnimatedChart(final boolean value) {
        this.positionChart.setAnimated(value);
        this.speedChart.setAnimated(value);
        this.accelerationChart.setAnimated(value);
    }
    
    /**
     * {@inheritDoc}
     */
    public void updateCharts(final double time, final double position, final double speed, final double acceleration) {
        this.updatePositionChart(time, position);
        this.updateSpeedChart(time, speed);
        this.updateAccelerationChart(time, acceleration);
    }
    
    /**
     * {@inheritDoc}
     */
    public void updateStatus(final String status) {
        this.label.setText(status);
    }
    
    /**
     * {@inheritDoc}
     */
    public void reset() {
        this.setAnimatedChart(false); //devo cambiare il valore dell'animazione per evitare bug quando pulisco i grafici
        this.clearSeries();
        this.clearCharts();
        this.setAnimatedChart(true);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        this.positions = new Series<>();
        this.positions.setName("Distance");
        this.speed = new Series<>();
        this.speed.setName("Speed");
        this.acceleration = new Series<>();
        this.acceleration.setName("Acceleration");        
        this.positionChart.getData().add(this.positions);
        this.speedChart.getData().add(this.speed);
        this.accelerationChart.getData().add(this.acceleration);
    }
}
