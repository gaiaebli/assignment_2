package controllers;

public interface PopupController {

    /**
     * What happens when the button is clicked.
     */
    void btnClick();
}
