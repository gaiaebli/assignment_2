package controllers;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class PopupControllerImpl implements PopupController{
    
    private final Controller controller;
    
    @FXML
    private Button btn;

    public PopupControllerImpl(final Controller controller) {
        this.controller = controller;
    }

    /**
     * {@inheritDoc}
     */
    @FXML
    public void btnClick() {
        this.controller.endExperiment();
        final Stage stage = (Stage) this.btn.getScene().getWindow(); //salvo questo stage
        stage.close(); //chiude questo popup
    }
    
}
