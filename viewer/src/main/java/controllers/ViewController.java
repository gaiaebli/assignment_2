package controllers;

public interface ViewController {

    /**
     * Updates the three charts with the current values.
     * @param time
     *      current time
     * @param position
     *      current position
     * @param speed
     *      current speed
     * @param acceleration
     *      current acceleration
     */
    void updateCharts(double time, double position, double speed, double acceleration);
    
    /**
     * Updates the view's status label. 
     * @param status
     *      the current status
     */
    void updateStatus(String status);
    
    /**
     * Resets the charts.
     */
    void reset();
}
