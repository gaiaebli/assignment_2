package controllers;

public interface Controller {

    /**
     * Starts the loop that controls the application.
     */
    void start();
    
    /**
     * Ends the current executing experiment.
     */
    void endExperiment();
}
