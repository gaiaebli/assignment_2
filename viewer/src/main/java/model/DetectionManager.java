package model;

public interface DetectionManager {

    /**
     * Adds new data.
     * @param pos
     *      new position
     */
    void addData(double pos);
    
    /**
     * Sets the period's value.
     * @param value
     *      period's value
     */
    void setPeriod(long value);
    
    /**
     * Returns the last position.
     * @return the last position
     */
    double getPosition();
    
    /**
     * Return speed's last value.
     * @return speed's last value
     */
    double getSpeed();
    
    /**
     * Return acceleration's last value.
     * @return acceleration's last value
     */
    double getAcceleration();
    
    /**
     * Returns the current time.
     * @return the current time
     */
    double getTimeInSeconds();
    
    /**
     * Resets all the experiment's values.
     */
    void reset();
}
