package model;

public class DetectionManagerImpl implements DetectionManager{

    private long period;
    private double time;
    private double lastPos;
    
    public DetectionManagerImpl() {
        this.period = 0;
        this.time = 0;
    }

    /**
     * {@inheritDoc}
     */
    public void addData(final double pos) {
        this.time += this.period; //aggiorno il tempo corrente aggingendo il tempo trascorso ovvero il periodo
        this.lastPos = pos; //mi salvo la posizione corrente per calcolare velocità e accelerazione
    }
    
    /**
     * {@inheritDoc}
     */
    public void setPeriod(final long value) {
        this.period = value;
    }
    
    /**
     * {@inheritDoc}
     */
    public double getPosition() {
        return this.lastPos;
    }
    
    /**
     * {@inheritDoc}
     */
    public double getSpeed() {
        return this.calculateSpeed();
    }
    
    /**
     * {@inheritDoc}
     */
    public double getAcceleration() {
        return this.calculateAcceleration();
    }
    
    /**
     * {@inheritDoc}
     */
    public double getTimeInSeconds() {
        return this.time/1000; //restituisce il tempo in secondi
    }
    
    /**
     * {@inheritDoc}
     */
    public void reset() {
        this.time = 0;
    }
    
    /**
     * Computes the speed's value based on the current time.
     * @return
     *      the speed's value
     */
    private double calculateSpeed() {
        return this.lastPos/this.time*1000; //calcola la velocità in cm/s
    }
    
    /**
     * Computes the acceleration's value based on the current time.
     * @return
     *      the acceleration's value
     */
    private double calculateAcceleration() {
        return this.calculateSpeed()/this.time*1000; //calcola l'accelerazione in cm/s^2
    }
    
}
