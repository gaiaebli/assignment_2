package model;

public interface Model {

    /**
     * Returns the current detection manager.
     * @return the current detection manager
     */
    DetectionManager getDetManager();
    
    /**
     * Returns the last message received from Arduino's serial.
     * @return the message received
     * @throws InterruptedException
     */
    String getMessage() throws InterruptedException;
    
    /**
     * Sends a message to Arduino through serial.
     * @param message
     *      the message to send
     */
    void sendMessage(String message);
    
    /**
     * Resets the experiment.
     */
    void reset();
}
