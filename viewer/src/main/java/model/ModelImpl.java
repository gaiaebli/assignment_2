package model;

public class ModelImpl implements Model {
    
    private final DetectionManager detManager;
    private final CommChannel channel;
    
    public ModelImpl() throws Exception {
        this.channel = new SerialCommChannel("COM3",9600);
        this.detManager = new DetectionManagerImpl();
    }
    
    /**
     * {@inheritDoc}
     */
    public DetectionManager getDetManager() {
        return this.detManager;
    }
    
    /**
     * {@inheritDoc}
     */
    public String getMessage() throws InterruptedException {
        return this.channel.receiveMsg();
    }
    
    /**
     * {@inheritDoc}
     */
    public void sendMessage(final String message) {
        this.channel.sendMsg(message);
    }

    /**
     * {@inheritDoc}
     */
    public void reset() {
        this.detManager.reset();
    }
}
